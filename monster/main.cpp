#include "monster.h"
#include <cstring>
#include <csignal>

SEMAPHORE sem_server;
SEMAPHORE sem_server2;
SEMAPHORE sem_player;
SEMAPHORE sem_player2;
SHM Lmem;

sem_t semmove;
sem_t sempaint;
bool wasMove = false;

Map map;
Player *player;
int gameOver = false;

SEMAPHORE ssem;
SEMAPHORE psem;
SHM Pmem;

struct link_message *lmem;

void* game_clock (void* arg) {
    struct timespec ts{};
    while (true) {
        if (clock_gettime (CLOCK_REALTIME, & ts) == -1) {
            perror ("clock_gettime");
            exit (EXIT_FAILURE);
        }
        ts.tv_sec += 3;
        while (ssem.timedwait(ts) == -1) {
            int memStatus = kill (player->serverPID, 0);
            if (memStatus) {
                gameOver = true;
                sem_post (&sempaint);
                return nullptr;
            }
            ts.tv_sec += 3;
        }
        player->lmap.update();
        map.update_player (player, 0);
        if (wasMove) {
            wasMove = false;
            sem_post (&sempaint);
        }
    }
}

int choose_event () {
    static int a = player->PID;
    static int choose = 0;
    int lastMove = choose;
    int b = 2;
    srand (time (nullptr) * time (nullptr) / 3 + a);
    if (player->slowdown) {
        return 0;
    }

    Point pos(player->currentPosition.y, player->currentPosition.x);

    while (true) {
        int r = (rand () * rand () + a - 13) % 10;
        if (!r) {
            return 0;
        }

        int findPlayer = map.aim_player (pos);

        switch (findPlayer) {
            case -1:
                break;
            case 0:
                return KEY_UP;
            case 1:
                return KEY_RIGHT;
            case 2:
                return KEY_DOWN;
            case 3:
                return KEY_LEFT;
            default:
                break;
        }

        switch (choose) {
            case 0:
                if (map.get_object (Point (pos.y, pos.x + 1)) != _WALL && /*!map.get_object (Point (pos.y, pos.x + 1)).is_player() &&*/ (lastMove != 3 || !b)) {
                    choose = 1;
                    return KEY_RIGHT;
                }
                if (map.get_object (Point (pos.y - 1, pos.x)) != _WALL && /*!map.get_object (Point (pos.y - 1, pos.x)).is_player() &&*/ (lastMove != 2 || !b)) {
                    choose = 0;
                    return KEY_UP;
                }
            case 1:
                if (map.get_object (Point (pos.y + 1, pos.x)) != _WALL && /*!map.get_object (Point (pos.y + 1, pos.x)).is_player() &&*/ (lastMove != 0 || !b)) {
                    choose = 2;
                    return KEY_DOWN;
                }
                if (map.get_object (Point (pos.y, pos.x + 1)) != _WALL && /*!map.get_object (Point (pos.y, pos.x + 1)).is_player() &&*/ (lastMove != 3 || !b)) {
                    choose = 1;
                    return KEY_RIGHT;
                }
            case 2:
                if (map.get_object (Point (pos.y, pos.x - 1)) != _WALL && /*!map.get_object (Point (pos.y, pos.x - 1)).is_player() &&*/ (lastMove != 1 || !b)) {
                    choose = 3;
                    return KEY_LEFT;
                }
                if (map.get_object (Point (pos.y + 1, pos.x)) != _WALL && /*!map.get_object (Point (pos.y + 1, pos.x)).is_player() &&*/ (lastMove != 0 || !b)) {
                    choose = 2;
                    return KEY_DOWN;
                }
            case 3:
                if (map.get_object (Point (pos.y - 1, pos.x)) != _WALL && /*!map.get_object (Point (pos.y - 1, pos.x)).is_player() &&*/ (lastMove != 2 || !b)) {
                    choose = 0;
                    return KEY_UP;
                }
                if (map.get_object (Point (pos.y, pos.x - 1)) != _WALL && /*!map.get_object (Point (pos.y, pos.x - 1)).is_player() &&*/ (lastMove != 1 || !b)) {
                    choose = 3;
                    return KEY_LEFT;
                }
                choose = 0;
                --b;
            default:
                break;
        }
    }

}

int main() {
    sem_close (&semmove);
    sem_close (&sempaint);
    try {
        sem_server.open  (SERVER_ON, 0);
        sem_server2.open (SERVER_ON2, 0);
        sem_player.open  (PLAYER_JOIN, 0);
        sem_player2.open (PLAYER_JOIN2, 0);
        sem_init (&semmove, 1, 0);
        sem_init (&sempaint, 1, 0);
    } catch (...) {
        return 1;
    }

    try {
        Lmem.open (MEMORY, sizeof (struct link_message), (void**) & lmem, 0);
    } catch (...) {
        return 1;
    }

    int serverStatus = kill (lmem->serverPID, 0);
    if (serverStatus == -1) {
        return 1;
    }

    sem_server.wait();
    lmem->playerPID = getpid();
    lmem->type = MONSTER;
    sem_player.post();
    sem_server2.wait();

    Player connection;

    mempcpy (connection.ssemaphore, lmem->ssemaphore, MESSAGE_SIZE);
    mempcpy (connection.psemaphore, lmem->psemaphore, MESSAGE_SIZE);
    mempcpy (connection.memory,     lmem->memory,     MESSAGE_SIZE);
    sem_player2.post();

    try {
        ssem.open (connection.ssemaphore, 0);
        psem.open (connection.psemaphore, 0);
        Pmem.open (connection.memory, sizeof (struct Player), (void**) & player, 0);
    } catch (...) {
        return 1;
    }

    ssem.wait();

    map.add_player (player, 0);

    pthread_t gameClock;
    pthread_create (&gameClock, nullptr, game_clock, nullptr);

    while (true) {
        int post = 1;
        player->direction = Point (0,  0);
        int c = choose_event ();
        if (gameOver) {
            break;
        }
        player->end  = false;
        switch (c) {
            case KEY_UP:
                player->direction = Point (-1,  0);
                break;
            case KEY_DOWN:
                player->direction = Point ( 1,  0);
                break;
            case KEY_LEFT:
                player->direction = Point ( 0, -1);
                break;
            case KEY_RIGHT:
                player->direction = Point ( 0,  1);
                break;
            case 0:
                player->direction = Point (0, 0);
                break;
            default:
                post = 0;
                break;
        }
        if (post) {
            psem.post();
            wasMove = true;
            sem_wait (&sempaint);
        }
    }

    return 0;
}
