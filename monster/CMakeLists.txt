cmake_minimum_required(VERSION 3.16.3)
project(monster)

set(CMAKE_CXX_STANDARD 17)

ADD_LIBRARY(LibsModule main.cpp monster.h)
target_link_libraries(LibsModule -lpthread -lrt -lncursesw)

add_executable(monster main.cpp)

target_link_libraries(monster LibsModule)