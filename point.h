//
// Created by Lenovo on 22.01.2021.
//

#ifndef POINT_H
#define POINT_H

#include "sout.h"
/*------------------------------------------------------------------------------------------*/
class Point {
public:
    int y;
    int x;

    Point (int _y = 0, int _x = 0) : y(_y), x(_x) {};
    Point (const Point& p2) = default;
    ~Point () = default;
    bool operator== (const Point& p2) const;
    Point& operator= (const Point& p2) = default;
    Point& set (const int _y, const int _x);
    Point& operator+= (const Point& p2);
    Point operator+ (const Point& p2) const;
    Point& operator-= (const Point& p2);
    Point operator- (const Point& p2) const;
    Point& moveRight ();
    Point& moveLeft ();
    Point& moveUp ();
    Point& moveDown ();
    Point & random (const int &xRange, const int &yRange);
    friend Sout & operator<< (Sout &out, const Point & point) {
        out << "x:" << point.x << " y:" << point.y;
        return out;
    }
};
/*------------------------------------------------------------------------------------------*/
bool Point::operator== (const Point& p2) const {   //= default;
    if (x == p2.x && y == p2.y) {
        return true;
    }
    return false;
}

Point& Point::set (const int _y, const int _x) {
    y = _y;
    x = _x;
    return *this;
}

Point& Point::operator+= (const Point& p2) {
    x += p2.x;
    y += p2.y;
    return *this;
}

Point Point::operator+ (const Point& p2) const {
    Point result (y, x);
    return result += p2;
}

Point& Point::operator-= (const Point& p2) {
    x -= p2.x;
    y -= p2.y;
    return *this;
}

Point Point::operator- (const Point& p2) const {
    Point result (y, x);
    return result -= p2;
}

Point& Point::moveRight () {
    ++x;
    return *this;
}

Point& Point::moveLeft () {
    --x;
    return *this;
}

Point& Point::moveUp () {
    --y;
    return *this;
}

Point& Point::moveDown () {
    ++y;
    return *this;
}

Point & Point::random (const int &xRange, const int &yRange) {
    int static n = 0;
    srand (time (NULL) * time (NULL) + n++);
    x = rand() % xRange;
    y = rand() % yRange;
    return *this;
}

#endif // POINT_H